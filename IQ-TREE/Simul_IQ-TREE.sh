#!/bin/bash

#simulation des 1000 topologies via IQTREE, longueurs de branches estimées 
#sur des données réelles correspondant à des arbres construits à partir de 
#séquences de riboprotéines de Gammaprotéobactéries. 

mkdir file_ctrl
for ((i=1; i<=1000; i++))
do
   
   iqtree -r 2000 -rlen 0.0 0.01892559139440915 0.568045425 file_ctrl/tree$i
   
done

