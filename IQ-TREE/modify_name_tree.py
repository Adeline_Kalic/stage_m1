#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''
Les noms de fichiers des topologies de IQ-TREE sont renommés suivant le même 
nombre étant inclu dans les noms de fichiers des jeux de séquences après shuffling. 
Ceci afin de savoir à quelle topologie appartient un jeu de séquences. 

'''

import os

lt=[]

with open('data_tree.txt', 'r') as infile:
    for l in infile.readlines():
        lt.append(l.split( )[2])
        
print(lt)

for i in range(1,1001):
    #print("tree"+str(i)+" "+lt[i-1])
    
    os.rename("tree"+str(i),"treemodifs"+str(i))


for i in range(1,1001):
    #print("tree"+str(i)+" "+lt[i-1])
    
    os.rename("treemodifs"+str(i),lt[i-1])

