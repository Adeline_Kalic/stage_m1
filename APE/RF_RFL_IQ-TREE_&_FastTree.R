df_identifyc_tree <- read.csv("identifyctree2.csv",header = FALSE,col.names=c("filename","alpha"))

#distance RF classique (method =  PH85)
#distance RFL (method = score)

a0.25<-subset(df_identifyc_tree,df_identifyc_tree$alpha=="alpha=0.25",filename)
a0.5<-subset(df_identifyc_tree,df_identifyc_tree$alpha=="alpha=0.5",filename)
a1<-subset(df_identifyc_tree,df_identifyc_tree$alpha=="alpha=1.0",filename)
a2<-subset(df_identifyc_tree,df_identifyc_tree$alpha=="alpha=2.0",filename)
a5<-subset(df_identifyc_tree,df_identifyc_tree$alpha=="alpha=5.0",filename)

#initialisation du dataframe qui contiendra les distances RF/RFL
df <- data.frame(Doubles=double(),
                 Ints=integer(),
                 Factors=factor(),
                 Logicals=logical(),
                 Characters=character(),
                 stringsAsFactors=FALSE)

colnames(df) <- c("filename","RFL","RF","a")

##pour alpha=0.25
for (i in 1L:nrow(a0.25)){
  tree_ref<-read.tree(paste("1000-topologies-2/tree",a0.25[i,],sep=""))
  tree_fasttree<-read.tree(paste("Guide_trees/seq",a0.25[i,],"_TRUE.fsa.fasttree.new",sep=""))
  RF<-dist.topo(tree_ref, tree_fasttree, method = "PH85")
  RF_normalise<-as.numeric(RF)/3994
  RFL<-dist.topo(tree_ref, tree_fasttree, method = "score")
  RFa<-data.frame(paste("tree",a0.25[i,],sep=""),format(round(RFL[1:1],4),nsmall=4),format(round(RF_normalise[1:1],4),nsmall=4),format(round(0.25, 2), nsmall = 2),stringsAsFactors=FALSE)
  colnames(RFa) <- c("filename","RFL","RF","a")
  df <- rbind(df, RFa)
  
}

for (i in 1L:nrow(a0.5)){
  tree_ref<-read.tree(paste("1000-topologies-2/tree",a0.5[i,],sep=""))
  tree_fasttree<-read.tree(paste("Guide_trees/seq",a0.5[i,],"_TRUE.fsa.fasttree.new",sep=""))
  RF<-dist.topo(tree_ref, tree_fasttree, method = "PH85")
  RF_normalise<-as.numeric(RF)/3994
  RFL<-dist.topo(tree_ref, tree_fasttree, method = "score")
  RFa<-data.frame(paste("tree",a0.5[i,],sep=""),format(round(RFL[1:1],4),nsmall=4),format(round(RF_normalise[1:1],4),nsmall=4),format(round(0.5, 2), nsmall = 2),stringsAsFactors=FALSE)
  colnames(RFa) <- c("filename","RFL","RF","a")
  df <- rbind(df, RFa)
  
}

for (i in 1L:nrow(a1)){
  tree_ref<-read.tree(paste("1000-topologies-2/tree",a1[i,],sep=""))
  tree_fasttree<-read.tree(paste("Guide_trees/seq",a1[i,],"_TRUE.fsa.fasttree.new",sep=""))
  RF<-dist.topo(tree_ref, tree_fasttree, method = "PH85")
  RF_normalise<-as.numeric(RF)/3994
  RFL<-dist.topo(tree_ref, tree_fasttree, method = "score")
  RFa<-data.frame(paste("tree",a1[i,],sep=""),format(round(RFL[1:1],4),nsmall=4),format(round(RF_normalise[1:1],4),nsmall=4),format(round(1, 2), nsmall = 2),stringsAsFactors=FALSE)
  colnames(RFa) <- c("filename","RFL","RF","a")
  df <- rbind(df, RFa)
  
}

for (i in 1L:nrow(a2)){
  tree_ref<-read.tree(paste("1000-topologies-2/tree",a2[i,],sep=""))
  tree_fasttree<-read.tree(paste("Guide_trees/seq",a2[i,],"_TRUE.fsa.fasttree.new",sep=""))
  RF<-dist.topo(tree_ref, tree_fasttree, method = "PH85")
  RF_normalise<-as.numeric(RF)/3994
  RFL<-dist.topo(tree_ref, tree_fasttree, method = "score")
  RFa<-data.frame(paste("tree",a2[i,],sep=""),format(round(RFL[1:1],4),nsmall=4),format(round(RF_normalise[1:1],4),nsmall=4),format(round(2, 2), nsmall = 2),stringsAsFactors=FALSE)
  colnames(RFa) <- c("filename","RFL","RF","a")
  df <- rbind(df, RFa)
  
}

for (i in 1L:nrow(a5)){
  tree_ref<-read.tree(paste("1000-topologies-2/tree",a5[i,],sep=""))
  tree_fasttree<-read.tree(paste("Guide_trees/seq",a5[i,],"_TRUE.fsa.fasttree.new",sep=""))
  RF<-dist.topo(tree_ref, tree_fasttree, method = "PH85")
  RF_normalise<-as.numeric(RF)/3994
  RFL<-dist.topo(tree_ref, tree_fasttree, method = "score")
  RFa<-data.frame(paste("tree",a5[i,],sep=""),format(round(RFL[1:1],4),nsmall=4),format(round(RF_normalise[1:1],4),nsmall=4),format(round(5, 2), nsmall = 2),stringsAsFactors=FALSE)
  colnames(RFa) <- c("filename","RFL","RF","a")
  df <- rbind(df, RFa)
  
}

tapply(as.numeric(df$RFL),df$a,mean)
tapply(as.numeric(df$RF),df$a,mean)

write.csv(df,file="distance.csv")

aovRFL <- aov(as.numeric(RFL) ~ a, data=df) 
summary(aovRFL)

aovRF <- aov(as.numeric(RF) ~ a, data=df)
summary(aovRF)

library(ggplot2)

ggplot(df ,aes(x=a, y=as.numeric(RF), colour=a,fill=a))+
  ggtitle("Distance RF en fonction de alpha")+
  ylab("RF")+
  scale_x_discrete(labels=c("0.25" = "Classe 1", "0.50" = "Classe 2","1.00" = "Classe 3", "2.00" = 
                              "Classe 4", "5.00" = "Classe 5"))+
  geom_jitter(width=0.25)+ #disperse les points sur toute la largeur
  geom_boxplot(alpha=0.25, outlier.alpha=0) + 
  stat_summary(fun.y=mean, colour="black", geom="point",
               shape=18, size=3,show.legend = FALSE)+
  theme_classic()+
  theme(legend.position="none")+
  theme(axis.text = element_text(angle=30, hjust=1, vjust=1))+
  theme(plot.title = element_text(hjust=0.5))


ggplot(df ,aes(x=a, y=as.numeric(RFL), colour=a,fill=a))+
  ggtitle("Distance RFL en fonction de alpha")+
  ylab("RFL")+
  scale_x_discrete(labels=c("0.25" = "Classe 1", "0.50" = "Classe 2","1.00" = "Classe 3", "2.00" = 
                              "Classe 4", "5.00" = "Classe 5"))+
  geom_jitter(width=0.25)+ #disperse les points sur toute la largeur
  geom_boxplot(alpha=0.25, outlier.alpha=0) + 
  stat_summary(fun.y=mean, colour="black", geom="point",
               shape=18, size=3,show.legend = FALSE)+
  theme_classic()+
  theme(legend.position="none")+
  theme(axis.text = element_text(angle=30, hjust=1, vjust=1))+
  theme(plot.title = element_text(hjust=0.5))









