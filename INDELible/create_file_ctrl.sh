#!/bin/bash

#création des 1000 fichiers control.txt qui serviront à générer les séquences 
#protéiques pour chaque arbre suivant le modèle LG et le modèle d'indel


for ((i=1; i<=200; i++))
do
   touch file_ctrl/Control$i.txt 
   echo -e "[TYPE] AMINOACID 1\n\n[MODEL] LG\n[submodel]  13\n[rates] 0.05 0.25 8\n[indelmodel] POW 1.8 6\n[insertrate] 0.00035\n[deleterate] 0.00105\n\n[TREE] LG.tree\n">>file_ctrl/Control$i.txt
   cat file_ctrl/tree$i >> file_ctrl/Control$i.txt
   echo -e "\n[PARTITIONS] p1  [LG.tree LG 5500]\n\n[SETTINGS]\n[output] FASTA\n[fastaextension] fsa\n[randomseed] 3267\n\n[EVOLVE] p1 1 seq$i" >> file_ctrl/Control$i.txt

done

for ((i=201; i<=400; i++))
do
   touch file_ctrl/Control$i.txt 
   echo -e "[TYPE] AMINOACID 1\n\n[MODEL] LG\n[submodel]  13\n[rates] 0.05 0.5 8\n[indelmodel] POW 1.8 6\n[insertrate] 0.00035\n[deleterate] 0.00105\n\n[TREE] LG.tree\n">>file_ctrl/Control$i.txt
   cat file_ctrl/tree$i >> file_ctrl/Control$i.txt
   echo -e "\n[PARTITIONS] p1  [LG.tree LG 5500]\n\n[SETTINGS]\n[output] FASTA\n[fastaextension] fsa\n[randomseed] 3267\n\n[EVOLVE] p1 1 seq$i" >> file_ctrl/Control$i.txt

done

for ((i=401; i<=600; i++))
do
   touch file_ctrl/Control$i.txt 
   echo -e "[TYPE] AMINOACID 1\n\n[MODEL] LG\n[submodel]  13\n[rates] 0.05 1 8\n[indelmodel] POW 1.8 6\n[insertrate] 0.00035\n[deleterate] 0.00105\n\n[TREE] LG.tree\n">>file_ctrl/Control$i.txt
   cat file_ctrl/tree$i >> file_ctrl/Control$i.txt
   echo -e "\n[PARTITIONS] p1  [LG.tree LG 5500]\n\n[SETTINGS]\n[output] FASTA\n[fastaextension] fsa\n[randomseed] 3267\n\n[EVOLVE] p1 1 seq$i" >> file_ctrl/Control$i.txt

done

for ((i=601; i<=800; i++))
do
   touch file_ctrl/Control$i.txt 
   echo -e "[TYPE] AMINOACID 1\n\n[MODEL] LG\n[submodel]  13\n[rates] 0.05 2 8\n[indelmodel] POW 1.8 6\n[insertrate] 0.00035\n[deleterate] 0.00105\n\n[TREE] LG.tree\n">>file_ctrl/Control$i.txt
   cat file_ctrl/tree$i >> file_ctrl/Control$i.txt
   echo -e "\n[PARTITIONS] p1  [LG.tree LG 5500]\n\n[SETTINGS]\n[output] FASTA\n[fastaextension] fsa\n[randomseed] 3267\n\n[EVOLVE] p1 1 seq$i" >> file_ctrl/Control$i.txt

done

for ((i=801; i<=1000; i++))
do
   touch file_ctrl/Control$i.txt 
   echo -e "[TYPE] AMINOACID 1\n\n[MODEL] LG\n[submodel]  13\n[rates] 0.05 5 8\n[indelmodel] POW 1.8 6\n[insertrate] 0.00035\n[deleterate] 0.00105\n\n[TREE] LG.tree\n">>file_ctrl/Control$i.txt
   cat file_ctrl/tree$i >> file_ctrl/Control$i.txt
   echo -e "\n[PARTITIONS] p1  [LG.tree LG 5500]\n\n[SETTINGS]\n[output] FASTA\n[fastaextension] fsa\n[randomseed] 3267\n\n[EVOLVE] p1 1 seq$i" >> file_ctrl/Control$i.txt

done
