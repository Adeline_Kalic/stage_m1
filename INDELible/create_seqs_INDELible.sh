#!/bin/bash

#génération des 1000 jeux de données de séquences à partir des 1000 fichiers 
#control.txt

mkdir seq_w_indelible
for ((i=1; i<=1000; i++))
do
   cp file_ctrl/Control$i.txt seq_w_indelible/
   mv seq_w_indelible/Control$i.txt seq_w_indelible/Control.txt
   cd seq_w_indelible/
   indelible 
   cd ..
done
