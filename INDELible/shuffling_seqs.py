#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''
Permet le mélange des 1000 jeux de données de séquences afin de ne pas avoir
pour chaque paquet de 200 séquences une valeur de alpha identique. Pour chaque
jeu de données, la valeur de alpha est indiquée dans un fichier avec le nom 
du fichier correspondant.
'''

# 0.25 / 0.5 / 1.0 / 2.0 / 5.0 pr 200 à chq fois

import os
import random

randlist = random.sample(range(1,1001), 1000)

for i in range(1,1001):
    os.rename("seq"+str(i)+"_TRUE.fsa","seq_avant_chgts"+str(i)+"_TRUE.fsa")
    
    
for i in range(1,201):
    os.rename("seq_avant_chgts"+str(i)+"_TRUE.fsa","seq"+str(randlist[i-1])+"_TRUE.fsa")
    modifs="seq_avant_chgts"+str(i)+"_TRUE.fsa"+" ==> "+"seq"+str(randlist[i-1])+"_TRUE.fsa"+" alpha=0.25"+"\n"
    with open("data.txt", "a") as fichier:
        fichier.write(modifs)
for i in range(201,401):
    os.rename("seq_avant_chgts"+str(i)+"_TRUE.fsa","seq"+str(randlist[i-1])+"_TRUE.fsa")
    modifs="seq_avant_chgts"+str(i)+"_TRUE.fsa"+" ==> "+"seq"+str(randlist[i-1])+"_TRUE.fsa"+" alpha=0.5"+"\n"
    with open("data.txt", "a") as fichier:
        fichier.write(modifs)
    
for i in range(401,601):
    os.rename("seq_avant_chgts"+str(i)+"_TRUE.fsa","seq"+str(randlist[i-1])+"_TRUE.fsa")
    modifs="seq_avant_chgts"+str(i)+"_TRUE.fsa"+" ==> "+"seq"+str(randlist[i-1])+"_TRUE.fsa"+" alpha=1.0"+"\n"
    with open("data.txt", "a") as fichier:
        fichier.write(modifs)   

for i in range(601,801):
    os.rename("seq_avant_chgts"+str(i)+"_TRUE.fsa","seq"+str(randlist[i-1])+"_TRUE.fsa")
    modifs="seq_avant_chgts"+str(i)+"_TRUE.fsa"+" ==> "+"seq"+str(randlist[i-1])+"_TRUE.fsa"+" alpha=2.0"+"\n"
    with open("data.txt", "a") as fichier:
        fichier.write(modifs)
            
for i in range(801,1001):
    os.rename("seq_avant_chgts"+str(i)+"_TRUE.fsa","seq"+str(randlist[i-1])+"_TRUE.fsa")
    modifs="seq_avant_chgts"+str(i)+"_TRUE.fsa"+" ==> "+"seq"+str(randlist[i-1])+"_TRUE.fsa"+" alpha=5.0"+"\n"
    with open("data.txt", "a") as fichier:
        fichier.write(modifs)