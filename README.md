# Simulation de topologies de référence

Les topologies de référence sont simulées avec le programme IQ-TREE. 

## Installation 

[Lien d'installation du programme IQ-TREE](http://www.iqtree.org/doc/Quickstart)

## Utilisation

Simulation de 1000 topologies de 2000 UTO avec des valeurs de longueurs de branches
définies à partir d'arbres de riboprotéines de Gammaprotéobactéries. 

```bash
./Simul_IQ-TREE.sh
```

# Simulation de séquences 

Les séquences protéiques sont simulées avec le programme INDELible. 

## Installation

[Lien d'installation du programme INDELible](http://abacus.gene.ucl.ac.uk/software/indelible/download.php)

## Utilisation

INDELible obtient les instructions à partir d'un fichier nommé control.txt qui 
doit se trouver dans le même répertoire que l'exécutable INDELible. 
Création de 1000 fichiers control.txt (dans chaque fichier, une topologie de 
référence) comportant le modèle LG, une loi gamma dont le paramètre de alpha
varie (0.25, 0.5, 1.0, 2.0 et 5.0), une longueur d'indel par la distribution de Zipf.
La longueur des séquences à la racine est de 5500 AA. 

```bash
./create_file_ctrl.sh
```

Génération des 1000 jeux de données de séquences à partir des 1000 fichiers 
control.txt.

```bash
./create_seqs_INDELible.sh
```

# Comparaison des topologies de référence avec les arbres inférés à partir des données simulées

Cette comparaison s'effectue avec les distances RF/RFL à l'aide de la bibliothèque
ape disponible dans R.

# Forme de l'arbre

Racinement au point moyen avec le programme Seaview.

## Installation

[Lien d'installation du programme Seaview](http://doua.prabi.fr/software/seaview)

## Utilisation

Par exemple, pour les topologies de référence : 

```bash
./rooting_mid_point_IQ-TREE.sh
```

# Forme de l'arbre - 2

Estimation de la symétrie d’un arbre à partir de la mesure de la variance du 
nombre de nœuds traversés depuis la racine jusqu’à chaque feuille.

## Utilisation

```python
var_nodes_tree.py
```












