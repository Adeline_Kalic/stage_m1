#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import Amalphy_Script_Tree as phylo

import numpy as np
import os.path
import csv
import re

def identify(path):
    items = os.listdir(path)

    newlist = []
    for names in items:
        if names.endswith(".tree"): 
            newlist.append(path+"/"+names)
    return newlist


def number_of_nodes(f):
    l=[]
    dic_var={}
    dic_sd={}
    dic_nb_nodes={}
    
    for file in files:
        l=[]

        number=re.findall('\d+', file)[0]

        with open(file,"r") as tree:
            tree=tree.readline()
        tree=phylo.readTree(tree) #lecture de l'arbre
        root=phylo.getRoot(tree) #position racine
        feuilles=phylo.getLeaves(tree,root) #feuilles de l'arbre
        
        for i in feuilles: #pour chaque feuille on compte le nombre de noeuds depuis la racine
            l.append(len(phylo.getNodesBetween(tree,1,i)))
          
        dic_nb_nodes["nb_nodes_tree"+str(number)]=l
        #print(l)
        #print(dic_nb_nodes)
        dic_var["var_tree"+str(number)]=np.var(l)
        dic_sd["sd_tree"+str(number)]=np.std(l)
        
        
    return(dic_nb_nodes,dic_var,dic_sd)


if __name__ == '__main__':
    
    files=identify("root")

    dic_nb_nodes,dic_var,dic_sd=number_of_nodes(files)
    #print(dic_nb_nodes)
    
    with open('dict_nb_nodes.csv', 'w') as csv_file3:
        writer3 = csv.writer(csv_file3)
        for key, value in dic_nb_nodes.items():
            writer3.writerow([key, value])
    
    with open('dict_var.csv', 'w') as csv_file:
        writer = csv.writer(csv_file)
        for key, value in dic_var.items():
            writer.writerow([key, value])
            
    with open('dict_sd.csv', 'w') as csv_file2:
        writer2 = csv.writer(csv_file2)
        for key, value in dic_sd.items():
            writer2.writerow([key, value])
    
    
    
    
