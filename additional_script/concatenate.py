#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#pip install docopt

"""

Usage:
  sgap.py <path> 
  sgap.py (-h | --help)

Arguments:
  <path>       Directory containing aligned sequences

Options:
  -h, --help     Show this screen.

"""

from Bio import SeqIO
import glob 
import os.path
from Bio.Seq import Seq
from docopt import docopt   
import shutil
import os
import subprocess

l2=[]

def identify1261pb(path):
    l = glob.glob(os.getcwd()+'/'+path+'/*') 

    for i in l: 
        for seq_record in SeqIO.parse(i, "fasta"):
            if seq_record.id not in l2:
                l2.append(seq_record.id)
    return(l2,l)
    
            
def gap(fi,fo):
    shutil.copyfile(fi, fo) #copie du fichier 

    lst = list(SeqIO.parse(fi, "fasta")) 

    species_in_file=[]

    for record in lst: 
        if record.id in nspecies: 
            species_in_file.append(record.id)
        gapseq=Seq(len(record.seq)*'-')
    
    for i in nspecies:
        if i not in species_in_file:
            with open(fo, "a") as nouveaufichier:
                nouveaufichier.write(">"+i+'\n'+str(gapseq)+'\n')  
                
                
def concatenate(fi):
    alignfiles=''
    for i in fi:
        
        p1 = subprocess.Popen(['ls',i],stdout=subprocess.PIPE,stdin=subprocess.PIPE,stderr=subprocess.PIPE)
        p2 = subprocess.Popen(["grep", 'modifs$'], stdin=p1.stdout, stdout=subprocess.PIPE, encoding='utf-8', stderr=subprocess.PIPE)
        p1.stdout.close()
        output = p2.communicate()[0].strip()

        if len(output) != 0:

            alignfiles+=output+","
            p2.stdout.close()
            
    alignfiles=alignfiles[:-1]
    l_alignfiles = alignfiles.split(",")
    #print(l_alignfiles)
    files=l_alignfiles[:-1]
    files = ','.join(files)
    
    last_file=str(l_alignfiles[-1])
    #print(last_file)
    
    os.system("/usr/local/bin/seaview -concatenate"+" "+files+" "+last_file)
    #print("seaview -concatenate"+" "+files+" "+last_file)
    
    #return(l_alignfiles)
    
    

if __name__ == '__main__':
    arguments = docopt(__doc__, version='1.0')

    nspecies,files=identify1261pb(arguments["<path>"])
    
    for i in files:
       gap(i,i+"modifs")
           
    nspecies,files=identify1261pb(arguments["<path>"])
    
    concatenate(files)
    print("The files are concatenated")
    
    

