#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#pip install docopt

"""Simulates the fact that certain ribosomal proteins are absent for certain organisms.

Usage:
  gap.py <path> <f> 
  gap.py (-h | --help)

Arguments:
  <path>        Directory containing aligned sequences
  <f>           % of sequences which will be replaced by gaps


Options:
  -h, --help     Show this screen.

"""

import random

from Bio import SeqIO
from Bio.Seq import Seq
from docopt import docopt  
from math import *  
import os.path


def identify(path):
    items = os.listdir(path)

    newlist = []
    for names in items:
        if names.endswith(".fsa"): 
            newlist.append(path+"/"+names)
    return newlist



def gap(fi,f,fo):
    freq=f
    lst = list(SeqIO.parse(fi, "fasta")) 
    num_to_sample=round((len(lst)*freq)/100) 

    sample = set(random.sample(range(len(lst)), num_to_sample)) #tire un nombre aléatoire 

    
    for idx, record in enumerate(lst): #enumerate permet de boucler et d'avoir un compteur automatique
        if idx in sample: #si l'index est le même que le nombre aléatoire tiré
            record.seq = Seq(len(record.seq) * "-") #on remplace par des gaps de la longueur de la séquence
        seqs=record.format("fasta").lstrip()
        with open(fo, "a") as nouveaufichier:
            nouveaufichier.write(seqs)
            
                   
if __name__ == '__main__':
    arguments = docopt(__doc__, version='1.0')
    #print(arguments)
    files=identify(arguments["<path>"])
    
    for i in files:
       gap(i,float(arguments["<f>"]),i+"%missing")
       
    

    
    